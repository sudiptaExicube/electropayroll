import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {store} from './src/Store/AppStore';
import {Provider, useDispatch, useSelector} from 'react-redux';
import {ProvideTheme} from './src/Constants/Theme/Theme';
import MainNavigation from './src/Navigations/MainNavigation';
import DrawerNavigation from './src/Navigations/DrawerNavigation';
import { EventRegister } from 'react-native-event-listeners';
// import SplashScreen from 'react-native-splash-screen';
import RNBootSplash from 'react-native-bootsplash';
import OneSignal from 'react-native-onesignal';
const App = () => {
  const [isloginValue, setLoginValue] = useState(false);
  const ONESIGNAL_APP_ID = 'cf6549d5-5c76-4c07-ad27-f4ddee0af2b6'

  // setFcmPlayerID
  useEffect(() => {
    // const listener = EventRegister.addEventListener('loginsuccess', (data) => {
    //   console.log("working event : ", data);
    //   // if(data == "true"){
    //     // setTimeout(() => {
    //       // setLoginValue(data)
    //       // RNBootSplash.hide({ fade: true });
    //       // RNBootSplash.
    //     // }, 500);
    //   // }
    // })


    // RNBootSplash.show();
  // SplashScreen.hide();
    
  }, [isloginValue]);

  useEffect(()=>{
    OneSignal.setAppId(ONESIGNAL_APP_ID);
    OneSignal.promptForPushNotificationsWithUserResponse()
  },[])

  return (




      <Provider store={store}>
       <ProvideTheme>
       <NavigationContainer >
         <MainNavigation />
         {/* {isloginValue == true ? 
            // <DrawerNavigation />
            <SuccessNavigator />
            :
             <MainNavigation />
          } */}
      
         </NavigationContainer>
       </ProvideTheme>
     </Provider>
     
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
