// App used colors
const Colors = {
  // red: 'red',
  // black: '#000',
  // white: '#fff',
  // gray:'gray',
  // darkGray:'darkgray',
  // lightGray:'lightgray'


  green: '#299c71',
  light_blue:'#5894EC',
  orange: "#fd7e14",
  black: '#000',
  white: '#fff',
  dark_grey: "#1e1e1e",
  middium_grey: "#bdbdbd",
  light_grey: "#ebf0f9",
  red:"#dc3545",
  yellow:"#ffc107",
  green:"#198754",
  text_underline:'#0B254B'
  
};

// Defined Theme. Do not edit theme. if you want to change color modify corresponding color in 'Colors' //

const darkTheme = {
  headerColor: Colors.green,
  headerTextColor:Colors.white,
  backgroundColor: Colors.black,
  shadeDark:Colors.white,
  shadeMedium:Colors.middium_grey,
  shadeLight:Colors.light_grey,
  textFontColor: Colors.white,
  buttonBackgroundColor:Colors.orange,
  buttonTextColor:Colors.white,
  sucessColor:Colors.green,
  warningColor:Colors.yellow,
  errorColor:Colors.red,
  textUnderlineColor:Colors.text_underline,
  whiteColor:Colors.white,
  blackColor:Colors.black,
  lightBlue:Colors.light_blue
};

const lightTheme = {
  headerColor: Colors.green,
  headerTextColor:Colors.white,
  backgroundColor: Colors.white,
  shadeDark:Colors.dark_grey,
  shadeMedium:Colors.middium_grey,
  shadeLight:Colors.light_grey,
  textFontColor: Colors.black,
  buttonBackgroundColor:Colors.orange,
  buttonTextColor:Colors.white,
  sucessColor:Colors.green,
  warningColor:Colors.yellow,
  errorColor:Colors.red,
  textUnderlineColor:Colors.text_underline,
  whiteColor:Colors.white,
  blackColor:Colors.black,
  lightBlue:Colors.light_blue
};



// Defined Theme. Do not edit theme. if you want to change color modify corresponding color in 'Colors' //

export {darkTheme, lightTheme};
