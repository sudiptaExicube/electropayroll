// App used client icon
const Icons={
  icon1:require('../assets/icons/user-online.png'),
  backicon:require('../assets/icons/left-arrow.png'),
  menu:require('../assets/icons/menu.png'),
}

// App used client images
const Images={
  dummy_user_image:require('../assets/images/user.png'),
  leaveApproveTask:require('../assets/images/leave-approved-request.png'),
  leaveApplication:require('../assets/images/leave-application.png'),
  attendance:require('../assets/images/attendance.png'),
  profile:require('../assets/images/profile.png'),
  user_dark:require('../assets/images/user-dark.png'),
  payslip:require('../assets/images/payslip.png'),
  leave:require('../assets/images/leave.png'),
  login_background_image: require('../assets/images/login_background.png'),
  datetime: require('../assets/images/datetime.png'),
  officehours: require('../assets/images/office-hours.png'),
  app_splash:require('../assets/images/app_splash.png'),
  app_splash_new:require('../assets/images/app_splash_new.png'),
  app_logo_icon:require('../assets/images/app_logo_icon.png'),
  splash_background:require('../assets/images/splash_background.jpg'),
  loader:require('../assets/images/loader.gif'),
  profileCover:require('../assets/images/profile-bg.jpeg'),
  profileuser:require('../assets/images/profile-user.png'),
  expenses:require('../assets/images/expenses.png'),
  assets:require('../assets/images/assets.png'),
  uploadPlaceholder:require('../assets/images/upload-placeholder.jpeg'),
  loan:require('../assets/images/pay.png'),
  loanApplication:require('../assets/images/loan-application.png'),
  applyLoan:require('../assets/images/rupee.png'),
  holiday:require('../assets/images/holiday.png'),
  
}

export {Icons,Images}