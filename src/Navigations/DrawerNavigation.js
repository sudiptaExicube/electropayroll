import React, { useEffect } from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { DashDetails, Dashboard, Initial, AttendanceScreen, LeavelistScreen, PayslipScreen, LoanMainScreen, ExpanseScreen, AssetsScreen } from '../Screens';
import { useTheme } from '../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../Constants/window';
// import Icon from 'react-native-vector-icons/MaterialIcons';
import { DrawerActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'

import { FontFamily, FontSize } from '../Constants/Fonts';
import { CardStyleInterpolators } from '@react-navigation/stack';
import { useSelector } from 'react-redux';
// import { DrawerActions } from 'react-navigation';
// import Extrascreen from '../Screens/DashboardDetails/DashboardDetails';
import { getDataWithOutToken, getDataWithToken, postWithToken } from '../Service/service';

const Drawer = createDrawerNavigator();
const CustomDrawerContent = props => {
  const { colorTheme } = useTheme();
  const [activeItem, setActiveitem] = React.useState('Dashboard');
  const { employeeDetails, token, employeeApiUrl } = useSelector(state => state.common);
  const [moduleData, setModuleData] = React.useState("");

  useEffect(() => {
    setActiveitem('Attendance');
    getAvailableModule()
  }, [activeItem]);

  clickIndividual_DrawerMenu = (screenName) => {
    setActiveitem(screenName)
    props.navigation.navigate(screenName);
  }


  const goNextScreen = (res) => {
  
    if (res?.Module == "Attendance") {
      props.navigation.navigate('AttendanceDashboard', { pData: res?.Submenu })
    } else if (res?.Module == "MyProfile") {
      props.navigation.navigate('ProfileScreen', { pData: res?.Submenu })
    } else if (res?.Module == "LeaveDetails") {
      props.navigation.navigate('LeavelistScreen', { pData: res?.Submenu })
    } else if (res?.Module == "Payslip") {
      props.navigation.navigate('PayslipScreen', { pData: res?.Submenu })
    } else if (res?.Module == "MyExpenses") {
      props.navigation.navigate('ExpanseScreen', { pData: res?.Submenu })
    } else if (res?.Module == "MyAssets") {
      props.navigation.navigate('AssetsScreen', { pData: res?.Submenu })
    } else if (res?.Module == "Loan") {
      props.navigation.navigate('LoanMainScreen', { pData: res?.Submenu })
    }

  }

  const getAvailableModule = () => {
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    // postWithToken(employeeApiUrl, 'GetModuleList', paramData)
    postWithToken(employeeApiUrl, 'GetModuleList', paramData)
      .then((resp) => {
        console.log("Module response : ", resp?.Data)
        if (resp.Status) {
          // let result = resp?.Data.reduce((acc, item) => {
          //   acc[item.Module] = item.IsActive;
          //   return acc;
          // }, {});
          console.log("menu result ======>", resp?.Data)
          setModuleData(resp?.Data);
          //  setloader(false);
        } else {
          showMsg(resp.msg);
          //setloader(false);
        }
      })
      .catch((error) => {
        //setloader(false);
        console.log("company error : ", error)
      })
  }

  return (
    <DrawerContentScrollView
      showsVerticalScrollIndicator={false}
      {...props}
      style={{ backgroundColor: colorTheme.shadeDark }}>
      <View style={{ width: '100%', height: windowHeight, flexDirection: 'column' }}>
        <View style={{ height: 120, flexDirection: 'column', justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, textAlign: 'center', fontFamily: FontFamily.bold, letterSpacing: 0.5, color: colorTheme.whiteColor }}>{employeeDetails?.EmployeeName}</Text>
              {/* <Text style={{textAlign:'center',fontFamily:FontFamily.regular,letterSpacing:0.5,color:colorTheme.whiteColor}}>Emp No - #{employeeDetails?.EmployeeId}</Text> */}
              <View style={{ marginTop: 3, backgroundColor: colorTheme.headerColor, padding: 2, paddingHorizontal: 8, justifyContent: 'center', alignItems: 'center', borderRadius: 12 }}>
                <Text style={{ color: colorTheme.buttonTextColor, fontSize: FontSize.f14, fontFamily: FontFamily.medium }}>
                  #Emp No - {employeeDetails?.EmployeeNo}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View>
        {moduleData != "" ?
          <FlatList
            data={moduleData.filter(module => module.IsActive)}
            renderItem={({ item }) => (
              <DrawerItem
                label={item?.ModuleName}
                icon={() => <Icon color={item.Module == activeItem ? colorTheme.lightBlue : colorTheme.whiteColor} size={FontSize.f16} name={'home'} style={{ marginLeft: 10 }} />}
                labelStyle={{ color: item.Module == activeItem ? colorTheme.lightBlue : colorTheme.whiteColor, fontSize: FontSize.f15, marginLeft: 0, paddingLeft: 0 }}
                onPress={() => {
                  setActiveitem(item?.Module)
                  clickIndividual_DrawerMenu('Dashboard');
                  goNextScreen(item)
                }}
                style={{ backgroundColor: item.Module == activeItem ? colorTheme.textUnderlineColor : null }}
              />
            )}
            // keyExtractor={(item) => item.id}
            //contentContainerStyle={styles.listContainer}
          />
         :null}

        </View>


      </View>



      {/* <View
        style={{width: '100%', height: windowHeight, flexDirection: 'column'}}>
        <View
          style={{
            width: '100%',
            height: 120,
            flexDirection: 'column',
            borderBottomWidth:1,
            borderBottomColor:colorTheme.shadeLight
          }}>
          <View
            style={{
              height: 90,
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="account-circle" size={70} color={colorTheme.textFontColor}/>
          </View>
          <View style={{height: 30, width: '100%',alignItems:'center'}}>
            <Text style={{color: colorTheme.textFontColor,fontFamily:FontFamily.bold}}>Pradip Mondal</Text>
          </View>
        </View>
        <View style={{width: '100%', flex: 1}}>
          <DrawerItem
            label="Home"
            labelStyle={{color: colorTheme.textFontColor, fontSize: FontSize.f13}}
            onPress={() => {
              props.navigation.navigate('DashDetails');
            }}
          />
          <DrawerItem
            label="Profile"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('ProfileScreen');
              }, 500);
            }}
          />
          <DrawerItem
            label="Payslip"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('PayslipScreen');
              }, 500);
            }}
          /> 
          <DrawerItem
            label="Leave list"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('LeavelistScreen');
              }, 500);
            }}
          /> 

          <DrawerItem
            label="Add leave"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('ApplyLeaveScreen');
              }, 500);
            }}
          /> 
          <DrawerItem
            label="Attendance Screen"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('AttendanceScreen');
              }, 500);
            }}
          /> 
          <DrawerItem
            label="ExpanseScreen Screen"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('ExpanseScreen');
              }, 500);
            }}
          /> 
          <DrawerItem
            label="Add expanse Screen"
            labelStyle={{color: colorTheme.textFontColor, fontSize:FontSize.f13}}
            onPress={() => {
              props.navigation.dispatch(DrawerActions.closeDrawer())
              setTimeout(() => {
                props.navigation.navigate('AddexpanseScreen');
              }, 100);
            }}
          /> 
        </View>
      </View> */}
    </DrawerContentScrollView>
  );
};


const DrawerNavigation = () => {
  const { colorTheme } = useTheme();
  return (
    <Drawer.Navigator
      // screenOptions={{
      //   // headerShadowVisible:false,
      //   headerShown:true,
      //   headerStyle: {
      //     backgroundColor: colorTheme.buttonBackgroundColor,
      //   },
      //   headerTintColor: colorTheme.textFontColor,
      //   drawerStyle: {
      //     width: '70%',
      //   },
      // }}
      screenOptions={{
        drawerPosition: 'left', headerShown: false,
        // cardStyleInterpolator:CardStyleInterpolators.forFadeFromCenter 
      }}

      drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Dashboard" component={Dashboard}
        options={{
          headerShown: false, headerStyle: {
            backgroundColor: colorTheme.backGroundColor
          }
        }}

      // options={{
      //   headerStyle: {
      //     backgroundColor: 'orange',
      //   },
      //   headerTintColor: 'black',
      //   // headerLeft:false,
      //   headerShown: true,
      //   headerRight: () => (
      //     <TouchableOpacity  onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
      //       {/* <Icon name="menu" size={30} color="black" /> */}
      //       <Icon name="account-circle" size={30} color={colorTheme.textFontColor}
      //       style={{marginRight:10}}/>
      //     </TouchableOpacity>
      //   ),
      // }}

      />
      <Drawer.Screen name="DashDetails" component={DashDetails} />
      <Drawer.Screen name="AttendanceScreen" component={AttendanceScreen} />
      <Drawer.Screen name="LeavelistScreen" component={LeavelistScreen} />
      <Drawer.Screen name="PayslipScreen" component={PayslipScreen} />

      {/* <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="Dashboard" component={Dashboard} /> */}

      {/* <Drawer.Screen
        name="Initial"
        component={Initial}
        options={{headerShown: false}}
      /> */}
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
