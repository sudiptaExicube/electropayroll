import React, { Component, useEffect, useState } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { View, Text, StyleSheet,ActivityIndicator,ScrollView, TouchableOpacity, FlatList, ToastAndroid, Modal, Pressable, Image, RefreshControl} from 'react-native';
import { useTheme } from '../../Constants/Theme/Theme';
import Styles from './Style';
import { ScreenLayout } from '../../Components';
import Icon from 'react-native-vector-icons/Ionicons';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { useDispatch, useSelector } from 'react-redux';
import { getDataWithToken, postWithToken } from '../../Service/service';
import { setleaveBalance, clearleaveBalance } from '../../Store/Reducers/LeaveReducer';
import { FontFamily } from '../../Constants/Fonts';
const LeaveBalanceScreen = props => {
  const { colorTheme } = useTheme()
  const styles = Styles();
  const dispatch = useDispatch();
  const { employeeDetails, employeeApiUrl, token } = useSelector(state => state.common);
  const { leaveBalance } = useSelector(state => state.leave);
  const isFocused = useIsFocused();
  const [modalVisible, setModalVisible] = useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const [loader, setloader] = React.useState(false);


  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const d = new Date();

  useEffect(() => {
    if(isFocused == true){
      fetchLeaveBalance(null)
    }
    
  }, [isFocused])

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      fetchLeaveBalance('pullToRefresh')
      setRefreshing(false)
    });
  }, []);

  fetchLeaveBalance = (event) => {
    event == null ? setloader(true):setloader(false)
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    
    postWithToken(employeeApiUrl, 'MyCurrentLeaveBalance', paramData)
      .then((resp) => {
        event == null ? setloader(false):setloader(false)
        if (resp.Status == true) {
          // showMsg(resp.msg);
          if (resp.Data) {
            console.log("Leave Balance data ===> ", resp?.Data);
            dispatch(setleaveBalance(resp.Data))
            //setModalVisible(!modalVisible);
          } else { dispatch(setleaveBalance([])) }
        } else {
          dispatch(setleaveBalance([]))
          showMsg(resp.msg)
        }
      })
      .catch((error) => {
        console.log("Leave balance api error : ", error)
      })
  }

  /* == Show Toast msg function == */
  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  const renderItem = ({ item }) => (
    <View style={styles.listMainContainer}>
      <View style={{ width: '15%', backgroundColor: "#051121", padding: 10, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 8, borderBottomLeftRadius: 8, paddingVertical: 20, }}>
        <Text style={{ color: '#fff', fontSize: 12, textTransform: 'uppercase' }}>{item?.AttendanceCode}</Text>
      </View>

      <View style={{ width: '85%', padding: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 13, color: "#000" }}>{item?.Opening}</Text>
          <Text style={{ fontSize: 12.5, color: "#A8A8A8" }}>Opening</Text>
        </View>
        <View style={{ height: 30, width: 1.5, backgroundColor: '#F0F0F0' }}></View>

        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 13, color: "#000" }}>{item?.Credit}</Text>
          <Text style={{ fontSize: 12.5, color: "#A8A8A8" }}>Credit</Text>
        </View>

        <View style={{ height: 30, width: 1.5, backgroundColor: '#F0F0F0' }}></View>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 13, color: "#000" }}>{item?.Debit}</Text>
          <Text style={{ fontSize: 12.5, color: "#A8A8A8" }}>Debit</Text>
        </View>
        <View style={{ height: 30, width: 1.5, backgroundColor: '#F0F0F0' }}></View>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 13, color: "#000" }}>{item?.Adjustment}</Text>
          <Text style={{ fontSize: 12.5, color: "#A8A8A8" }}>Adjustment</Text>
        </View>
        <View style={{ height: 30, width: 1.5, backgroundColor: '#F0F0F0' }}></View>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={{ fontSize: 13, color: "#000" }}>{item?.Balance}</Text>
          <Text style={{ fontSize: 12.5, color: "green" }}>Balance</Text>
        </View>
      </View>

    </View>
  );

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Leave Balance"
      headerbackClick={() => { props.navigation.goBack() }}>

      <View style={styles.centeredView}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Image source={Images.loader} resizeMode={"contain"} style={{
                width: 60, height: 60, borderRadius: 75 / 2,
                borderWidth: 3, overflow: 'hidden', overlayColor: 'white',
              }} />
            </View>
          </View>
        </Modal>

      </View>

      {leaveBalance.length > 0 ? 
      <View style={styles.container}>
        <View style={{paddingVertical:12}}><Text style={{ fontFamily: FontFamily.medium, color: '#000', fontSize:16 }}>{monthNames[d.getMonth()]} , {d.getFullYear()}</Text></View>
        <FlatList
          data={leaveBalance}
          renderItem={renderItem}
          keyExtractor={(item, index) => index}
          showsVerticalScrollIndicator={false}
          style={{ marginTop: 10, borderColor: '#004792', marginBottom: 80 }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
        />

      </View> : 
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          >
            <Text style={{  fontSize: 15, textAlign: 'center' }}>Sorry! No Result Found</Text>
          </ScrollView>
        }

      <Modal
          transparent={true}
          animationType={'none'}
          visible={loader}
          style={{ zIndex: 1100 }}
          onRequestClose={() => { }}>
          <View style={styles.modalBackground}>
            <View style={styles.activityIndicatorWrapper}>
              <ActivityIndicator animating={loader} color="black" size={30} />
            </View>
          </View>
        </Modal>
    </ScreenLayout>
  );
};

//make this component available to the app
export default LeaveBalanceScreen;
