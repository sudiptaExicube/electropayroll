import React, { Component, useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  ScrollView,
  SafeAreaView,
  Image,
  ToastAndroid,
  RefreshControl,
  ActivityIndicator,
  Modal
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import {
  setloantransactionData,
  clearloantransactionData
} from '../../Store/Reducers/LoanReducer';

import { getDataWithOutToken, getDataWithToken, postWithToken } from '../../Service/service';
import { DashDetails } from '../Screens';

import { useTheme } from '../../Constants/Theme/Theme';
import Styles from './Style';
import { ScreenLayout } from '../../Components';
import { useIsFocused } from '@react-navigation/native';
import { Images } from '../../Constants/ImageIconContant';
import Icon from 'react-native-vector-icons/Ionicons';
import { FontFamily } from '../../Constants/Fonts';

//import ImagePicker from 'react-native-image-picker';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
// create a component
const LoanMainScreen = props => {
  const { colorTheme } = useTheme()
  const styles = Styles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const [loader, setloader] = React.useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const { employeeDetails, employeeApiUrl, token } = useSelector(state => state.common);
  const { loantransactionList } = useSelector(state => state.loan);

  useEffect(() => {
    if (isFocused == true) {
      fetchExpenseHistory(null)
    }
  }, [isFocused])

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      fetchExpenseHistory('pullToRefresh')
      setRefreshing(false)
    });
  }, []);

  fetchExpenseHistory = (event) => {
    event == null ? setloader(true) : setloader(false)
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    postWithToken(employeeApiUrl, '/MyAdvanceTransactionList', paramData)
      .then((resp) => {
        event == null ? setloader(false) : setloader(false)
        if (resp.Status == true) {
          // showMsg(resp.msg);
          if (resp.Data) {
            console.log("Expense List ===> ", resp?.Data);
            console.log("================")
            dispatch(setloantransactionData(resp.Data))
           // setModalVisible(!modalVisible);
          } else { dispatch(setExpenseList([])) }
        } else {
          dispatch(setloantransactionData([]))
          showMsg(resp.msg)
        }
      })
      .catch((error) => {
        console.log("get expense api error : ", error)
        dispatch(setloantransactionData([]))
      })
  }

  /* == Show Toast msg function == */
  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  const renderItem = ({ item }) => (
    <View style={styles.listMainContainer}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 4 }}>
      <Text style={styles.listHeader}>Loan Amount : ₹ {item?.LoanAmount}</Text>
       
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start', marginTop: 0 }}>
          <Icon name="calendar-outline" size={18} color="#696969" />
          <Text style={styles.leaveRange}>{item?.LoanDate}</Text>
        </View>
      </View>
      <Text numberOfLines={2} style={styles.leaveReason}>Paid Amount: <Text style={{fontWeight:'bold'}}>₹{item?.PaidAmount}</Text></Text>
      <Text numberOfLines={2} style={styles.leaveReason}>EMI amount: <Text style={{fontWeight:'bold'}}>₹{item?.EMIAmount} X {item?.NoOfInstallments}</Text></Text>
      <Text numberOfLines={2} style={styles.leaveReason}>Payment Frequency: <Text style={{fontWeight:'bold'}}>{item?.Frequency}</Text></Text>
      <Text numberOfLines={2} style={styles.leaveReason}>Payment Start Month : <Text style={{fontWeight:'bold'}}>{item?.StartMonth}  |  Year: {item?.PaymentStartYear}</Text></Text>
      {item?.Status ?
        <View style={styles.border}></View>:null}
        {item?.Status ?
        <View style={styles.listFootersec}>
          <View style={styles.statusContainer}>
            <View style={[styles.statusIcon, { backgroundColor: colorTheme.lightBlue }]}></View>
            <Text style={[styles.statusText, { color: colorTheme.lightBlue }]}>{item?.Status}</Text>
          </View>
        </View> :null}
    </View>
    
  );

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Loan"
      headerbackClick={() => { props.navigation.goBack() }}
      showpowerButton={true}
      powerbuttonIconName="add-circle"
      powerbuttonIconSize={28}
      clickPowerbutton={() => { props.navigation.navigate('ApplyLoanScreen') }}
    >
      <View
        style={styles.container}>
        {loantransactionList.length > 0 ?
          <FlatList
            data={loantransactionList}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10, borderColor: '#004792', marginBottom: 80 }}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          /> :
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          >
            <Text style={{ marginTop: 180, fontSize: 15, textAlign: 'center' }}>Sorry! No Result Found</Text>
          </ScrollView>
        }
      </View>
      <Modal
        transparent={true}
        animationType={'none'}
        visible={loader}
        style={{ zIndex: 1100 }}
        onRequestClose={() => { }}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={loader} color="black" size={30} />
          </View>
        </View>
      </Modal>
    </ScreenLayout>
  );
};

//make this component available to the app
export default LoanMainScreen;
