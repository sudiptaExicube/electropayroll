
//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  FlatList
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './AttendanceDashboardStyle';
import { Images } from '../../Constants/ImageIconContant';
import { useDispatch, useSelector } from 'react-redux';
import { clearEmpAndCompanyDetails } from '../../Store/Reducers/CommonReducer';
import { deleteData, getData } from '../../Service/localStorage';

import { FontFamily, FontSize } from '../../Constants/Fonts';
import { colorTheme } from '../../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../../Constants/window';
import { useIsFocused } from '@react-navigation/native';

// create a component
const AttendanceDashboard = props => {
  const styles = Styles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  // const { companyApiUrl,employeeApiUrl,count } = useSelector(state => state.common);
  const { companyApiUrl, companyDetails, employeeApiUrl, employeeDetails, token } = useSelector(state => state.common);
  const [moduleData, setModuleData] = React.useState([1, 1, 1]);

  useEffect(() => {
    if (isFocused == true) {
      if (props?.route?.params?.pData) {
        let subMenuArr = props?.route?.params?.pData;
        console.log(subMenuArr);
        setModuleData(subMenuArr)
      }
    }
  }, [isFocused])


  /*== Logout Button click function === */
  const singOutBtn = () => {
    showConfirmDialog("Logout", "Are you sure you want to logout from this app?", "Logout", "LOGOUT")
  }

  /*== Logout functionality start === */
  const singOutFunc = () => {
    dispatch(clearEmpAndCompanyDetails())
    deleteData();
    setTimeout(() => {
      props.navigation.replace('CompanyLogin');
    }, 400);
  }

  /* == Show logout confirmation alert ==*/
  const showConfirmDialog = (title, body, actionText, type) => {
    return Alert.alert(
      title,
      body,
      [
        {
          text: actionText, onPress: () => {
            type == 'LOGOUT' ? singOutFunc() : BackHandler.exitApp();
          },
        },
        {
          text: "Cancel",
        },
      ]
    );
  };

  const goNextScreen = (res) => {
    if (res?.Module == "MyPunches") {
      //console.log(res?.Submenu)
      props.navigation.navigate('AttendanceScreen', { pData: res?.Submenu })
    } else if (res?.Module == "AttendanceReport") {
      props.navigation.navigate('AttendanceRepport', { pData: res?.Submenu })
    } else if (res?.Module == "AttendanceApplication") {
      props.navigation.navigate('Regularization', { pData: res?.Submenu })
    } else if (res?.Module == "OTApplication") {
      props.navigation.navigate('OTApplication', { pData: res?.Submenu })
    }
    //props.navigation.navigate()
  }

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Attendance"
      headerbackClick={() => { props.navigation.goBack() }}
      hamburgmenuVisable={false}
      // headermenuClick = {()=>{props.navigation.dispatch(DrawerActions.openDrawer())}}
      showpowerButton={true}
      clickPowerbutton={() => { singOutBtn() }}
    >
      <View style={styles.container}>
        <View style={styles.ProfileCard}>
          <Image source={Images.dummy_user_image} style={styles.profileImage} />
          <Text style={styles.userName}>{employeeDetails?.EmployeeName}</Text>
          {/* <Text style={styles.time}>Designation</Text> */}
        </View>
        {moduleData != "" ?
          <View style={styles.menuCard}>
            <FlatList
              data={moduleData.filter(module => module.IsActive)}
              renderItem={({ item }) => (

                <TouchableOpacity
                  style={styles.rightBox}
                  onPress={() =>
                    goNextScreen(item)
                  }>
                  <Image source={{ uri: item?.AndroidIconPath }} style={styles.cardIcon} />
                  <Text style={styles.cardText}>{item?.ModuleName}</Text>
                </TouchableOpacity>

              )}
              //keyExtractor={(item) => item.id}
              numColumns={2}
            />

          </View> : null}
      </View>
    </ScreenLayout>
  );
};



export default AttendanceDashboard;
//props.navigation.navigate('DashDetails')
