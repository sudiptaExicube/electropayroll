//import liraries
import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { FontFamily } from '../../../Constants/Fonts';
import { useTheme } from '../../../Constants/Theme/Theme';
import { FontSize } from '../../Constants/Fonts';
import { windowHeight,windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const {colorTheme} = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colorTheme.backgroundColor,
    },
    customScrollView:{ height:'100%', },
    customKeyboardAvoidingView:{ height:'100%'}, 
    backgroundImageView:{width: '100%', height: '100%'},
    mainView:{flex:1, flexDirection:'column',padding:20,paddingTop:200},
    headingContainer:{flex:1,flexDirection:'column',justifyContent:'flex-end',},
    headingText:{
      fontSize:18, color:'white',fontWeight:'600', 
      fontFamily:FontFamily.bold,
      letterSpacing:0.5
    },
    spaceView:{height:100},
    inputcontainerView:{flex:2},
    inputLable:{
      color:'white',fontSize:12,fontWeight:'400',letterSpacing:0.5,
      fontFamily:FontFamily.regular,
      paddingLeft:5
    },
    textinputField:{
      marginTop:0,padding:0,paddingBottom:15,fontWeight:'400',letterSpacing:0.5,
      fontFamily:FontFamily.regular,
      paddingLeft:5
    },
    forgotPassView:{marginTop:20,flexDirection:'row',justifyContent:'flex-end'},
    forgotPassText:{
      color:'white',fontSize:12,fontWeight:'700',letterSpacing:0.5,
      fontFamily:FontFamily.regular, 
      paddingLeft:5
    },
    submitView:{marginTop:30,flexDirection:'row',justifyContent:'center'},
    submitBtnView:{
      height:45,backgroundColor:colorTheme.headerColor,width:'100%',borderRadius:5,flexDirection:'column',justifyContent:'center'
    },
    submitBtnTextStyle:{
      color:'white',fontSize:12,fontWeight:'700',letterSpacing:0.5,
      fontFamily:FontFamily.regular, 
      paddingLeft:5,textAlign:'center'
    }




  });
};
export default Styles;