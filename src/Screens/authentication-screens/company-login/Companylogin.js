//import liraries
import React, { createRef, useEffect, useState } from 'react';
import { View, Text,BackHandler,Alert, ImageBackground, TextInput, TouchableOpacity, KeyboardAvoidingView, ToastAndroid, ActivityIndicator, ScrollView, SafeAreaView, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Styles from './Style';
import { ScreenLayout } from '../../../Components';
import { useTheme } from '../../../Constants/Theme/Theme';
import { Images } from '../../../Constants/ImageIconContant';
import { getDataWithOutToken, postWithOutToken } from '../../../Service/service';
import { setCompanyDetails } from '../../../Store/Reducers/CommonReducer';
import { useFocusEffect } from '@react-navigation/native';

const CompanyLogin = props => {
  const { colorTheme } = useTheme();
  const styles = Styles();
  const dispatch = useDispatch();
  // const {count} = useSelector(state => state.common);
  const [loadingValue, setLoading] = React.useState(false);
  
  // const [companyCode, setCompanycode] = React.useState('crpl');
  // const [companyPassword, setCompanypassword] = React.useState('crpl@123');

  const [companyCode, setCompanycode] = React.useState('');
  const [companyPassword, setCompanypassword] = React.useState('');
  const { companyApiUrl, employeeApiUrl } = useSelector(state => state.common);

  // ==> Button Focas reference create
  const companyPassRef = createRef()
  const companyCodeRef = createRef()

  useEffect(() => { 
   
  }, []);

  /* == For hardware back button functionality == */
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        console.log("back click working")
        showConfirmDialog( "Alert","Are you sure you want to exit from this app?", "Yes", "APPEXIT")
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

  /* == Show logout confirmation alert == */
  const showConfirmDialog = (title,body,actionText,type) => {
    return Alert.alert(
      title,
      body,
      [
        {
          text: actionText, onPress: () => {
            type == 'LOGOUT' ? singOutFunc() : BackHandler.exitApp();
          },
        },
        {
          text: "Cancel",
        },
      ]
    );
  };


  /* == Show Toast msg function == */
  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  /* == Verify button function == */
  const clickCompanyLogin = () => {
    console.log("companyApiUrl : ", companyApiUrl);
    setLoading(true);
    if (!companyCode) {
      setLoading(false);
      showMsg("Company code cannot be blank")
    } else if (!companyPassword) {
      setLoading(false);
      showMsg("Password field cannot be blank")
    } else {
      let paramData = {
        //"CompanyCode": "saral",
       // "Password": "1234",
         "CompanyCode":companyCode,
         "Password":companyPassword,
      }
      startCompanyLogin(paramData)
    }
  }

  /* == Company login Function == */
  const startCompanyLogin = (paramData) => {
    postWithOutToken(companyApiUrl, 'PayrollApiUrl', paramData)
      .then((resp) => {
        setLoading(false);
        console.log("company response : ", resp)
        if (resp.Status) {
          dispatch(setCompanyDetails(resp.Data))
          setTimeout(() => {
            props.navigation.replace('EmployeeLogin')
          }, 500);
        } else {
          showMsg(resp.msg)
        }
      })
      .catch((error) => {
        setLoading(false);
        console.log("company error : ", error)
      })
  }




  return (
    <ScreenLayout
      isHeaderShown={false}
      isShownHeaderLogo={false}
      headerTitle="SignIn">
      {/* <SafeAreaView style={{height:'100%'}}> */}
      <ImageBackground
        source={Images.login_background_image}
        style={styles.backgroundImageView}
      >
        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={35}
          style={styles.customKeyboardAvoidingView}>
          <ScrollView showsVerticalScrollIndicator={false} style={styles.customScrollView}>


            <View style={styles.mainView}>
              <View style={styles.headingContainer}>
                <View>
                  <Text style={styles.headingText}> Hello, Electro </Text>
                  <Text style={styles.headingText}> Welcome Back </Text>
                </View>
              </View>

              <View style={styles.spaceView}></View>

              <View style={styles.inputcontainerView}>
                <View>
                  <Text style={styles.inputLable}>Company code</Text>
                  <TextInput
                    // keyboardType='numeric'
                    value={companyCode}
                    onChangeText={text => setCompanycode(text)}
                    placeholder={'Company code'}
                    underlineColorAndroid={colorTheme.textUnderlineColor}
                    color={colorTheme.whiteColor}
                    placeholderTextColor={colorTheme.whiteColor}
                    style={styles.textinputField}
                    returnKeyType='next'

                    ref={companyCodeRef => this.companyCodeRef = companyCodeRef}
                    onSubmitEditing={() => this.companyPassRef.focus()}
                  >
                  </TextInput>

                </View>

                <View style={{ marginTop: 20 }}>
                  <Text style={styles.inputLable}>Password</Text>
                  <TextInput
                    secureTextEntry={true}
                    value={companyPassword}
                    onChangeText={text => setCompanypassword(text)}
                    placeholder={'Security pass'}
                    underlineColorAndroid={colorTheme.textUnderlineColor}
                    color={colorTheme.whiteColor}
                    placeholderTextColor={colorTheme.whiteColor}
                    style={styles.textinputField}
                    returnKeyType='done'
                    ref={companyPassRef => this.companyPassRef = companyPassRef}
                    onSubmitEditing={() => { }}
                  >
                  </TextInput>
                </View>

                {/* <View style={styles.forgotPassView}>
                  <TouchableOpacity onPress={() => { alert("Forgot password clicked") }}>
                    <Text style={styles.forgotPassText}>Forgot password?</Text>
                  </TouchableOpacity>
                </View> */}

                <TouchableOpacity style={styles.submitView} onPress={() => { !loadingValue && companyPassword.trim() && companyCode.trim() ? clickCompanyLogin() : null }}>
                  <View
                    style={[styles.submitBtnView, { opacity: companyPassword.trim() && companyCode.trim() ? 1 : 0.5 }]}
                    //onPress={() => { !loadingValue && companyPassword.trim() && companyCode.trim() ? clickCompanyLogin() : null }}
                    >
                    {loadingValue
                      ?
                      <ActivityIndicator
                        animating={true}
                        hidesWhenStopped={true}
                        color={colorTheme.whiteColor}
                      ></ActivityIndicator>
                      :
                      <Text style={styles.submitBtnTextStyle}>VERIFY</Text>
                    }
                    {/* <Text style={styles.submitBtnTextStyle}>VERIFY</Text> */}

                  </View>
                </TouchableOpacity>
              </View>
            </View>

          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
      {/* </SafeAreaView> */}
      {/* </ScrollView> */}
    </ScreenLayout>
  );
};

//make this component available to the app
export default CompanyLogin;
