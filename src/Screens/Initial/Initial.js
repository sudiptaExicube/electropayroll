//import liraries
import React, {useEffect} from 'react';
import {View, Text, StyleSheet, ImageBackground, Image, Dimensions, PermissionsAndroid} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ScreenLayout } from '../../Components';
import { Images } from '../../Constants/ImageIconContant';
import { useTheme } from '../../Constants/Theme/Theme';
import { getData } from '../../Service/localStorage';
import { 
  localstorage_CompanyDetailsAdd, 
  localstorage_EmployeeApiUrlAdd, 
  localstorage_EmployeeDetailsAdd, 
  localstorage_TokenAdd,
  clearEmpAndCompanyDetails, 
  setFcmPlayerID
} from '../../Store/Reducers/CommonReducer';

import Styles from './Style';
import RNBootSplash from 'react-native-bootsplash';
import OneSignal from 'react-native-onesignal';


// create a component
const Initial = props => {
  const {colorTheme} = useTheme();
  const styles=Styles()
  const dispatch = useDispatch();
  const ONESIGNAL_APP_ID = 'cf6549d5-5c76-4c07-ad27-f4ddee0af2b6'
  const { companyApiUrl,companyDetails,employeeApiUrl,token,fcmPlayerId } = useSelector(state => state.common);

  useEffect(() => {
    console.log("useeffect calling")

    OneSignal.setAppId(ONESIGNAL_APP_ID);
    findOnesignalPlayerId();
    // requestCameraPermission()

    setTimeout(() => {
      getData('token').then((tokenSuccess)=>{
        if(tokenSuccess){
          dispatch(localstorage_TokenAdd(tokenSuccess));

          getData('employeeData').then((empSucess)=>{
            if(empSucess){
              dispatch(localstorage_EmployeeDetailsAdd(JSON.parse(empSucess)))
              
              getData('companyDetails').then((companySuccess)=>{
                if(companySuccess){
                  dispatch(localstorage_CompanyDetailsAdd(JSON.parse(companySuccess)))

                  getData('employeeApiUrl').then((empUrlsuccess)=>{
                    if(empUrlsuccess){
                      dispatch(localstorage_EmployeeApiUrlAdd(empUrlsuccess))
                      offSplashScreen();
                      props.navigation.replace('Home');

                    }else{
                      dispatch(clearEmpAndCompanyDetails())
                      offSplashScreen();
                      props.navigation.replace('CompanyLogin');
                    }
                  })

                }else{
                  dispatch(clearEmpAndCompanyDetails())
                  offSplashScreen();
                  props.navigation.replace('CompanyLogin');
                }
              })

            }else{
              dispatch(clearEmpAndCompanyDetails())
              offSplashScreen();
              props.navigation.replace('CompanyLogin');
            }
          })

        }else{
          dispatch(clearEmpAndCompanyDetails())
          offSplashScreen();
          props.navigation.replace('CompanyLogin');
        }
        
      })      

    }, 200);

  }, []);

  /*
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
        {
          title: 'Electropayroll camera permission',
          message:
            'Electropayroll requires notification permission to send notification',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the Notification');
      } else {
        console.log('Notification permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  */
  
  /* == For finding onesignal player ID == */
  const findOnesignalPlayerId = async () => {
    try {
      const data = await OneSignal.getDeviceState();
      const player_id=data.userId;
      console.log("player_id :", player_id);
      if(player_id){
        dispatch(setFcmPlayerID(player_id))
      }
    } catch (error) {
      console.log(error);
    }
  };

  const offSplashScreen = () => {
    console.log("settimeout calling")
    setTimeout(() => {
      RNBootSplash.hide({fade:true});
    }, 1000);
  }

  return (

    <ScreenLayout
    isHeaderShown={false}
    isShownHeaderLogo={false}
    headerTitle="">
    <ImageBackground
      source={Images.app_splash}
      // source={Images.app_logo_icon}
      // source={Images.splash_background}
      
      
      style={styles.backgroundImageView}
    >
      
    </ImageBackground>
  </ScreenLayout>
  );
};


//make this component available to the app
export default Initial;
