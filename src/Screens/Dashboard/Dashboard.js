//import liraries
import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  BackHandler,
  ToastAndroid,
  Modal,
  ActivityIndicator,
  FlatList
} from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';

import { ScreenLayout } from '../../Components';

import Styles from './Style';
import { useTheme } from '../../Constants/Theme/Theme';
import { Icons, Images } from '../../Constants/ImageIconContant';
import { ColorSpace } from 'react-native-reanimated';

import { useDispatch, useSelector } from 'react-redux';
import { getDataWithOutToken, getDataWithToken, postWithToken } from '../../Service/service';
import {
  clearEmpAndCompanyDetails,
  setEmpBasicDetails,
  clearEmpBasicDetails,
} from '../../Store/Reducers/CommonReducer';
import { deleteData, getData } from '../../Service/localStorage';
import { useIsFocused } from '@react-navigation/native';

// create a component
const Dashboard = props => {
  const styles = Styles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  // const { companyApiUrl,employeeApiUrl,count } = useSelector(state => state.common);
  const { companyApiUrl, companyDetails, employeeApiUrl, employeeDetails, token, employeeBasicDetails, } = useSelector(state => state.common);

  const [fullDate, setDate] = useState("");
  const [currentTime, setTime] = useState("");
  const [loader, setloader] = React.useState(false);
  const [moduleData, setModuleData] = React.useState("");


  useEffect(() => {
    if (isFocused == true) {

      setloader(true)
      setInterval(() => {
        getCurrentDateTime();
      }, 1)
      fetchUserDetails('EmployeeBasicInfo');
    }

  }, [isFocused])

  /* == For hardware back button functionality == */
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        //console.log("back click working")
        showConfirmDialog("Alert", "Are you sure you want to exit from this app?", "Yes", "APPEXIT")
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );


  const getAvailableModule = () => {
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    // postWithToken(employeeApiUrl, 'GetModuleList', paramData)
    postWithToken(employeeApiUrl, 'GetModuleList', paramData)
      .then((resp) => {
        console.log("Module response : ", resp?.Data)
        if (resp.Status) {
          // let result = resp?.Data.reduce((acc, item) => {
          //   acc[item.Module] = item.IsActive;
          //   return acc;
          // }, {});
          console.log("result ======>", resp?.Data)
          setModuleData(resp?.Data);
          setloader(false);
        } else {
          showMsg(resp.msg);
          setloader(false);
        }
      })
      .catch((error) => {
        setloader(false);
        console.log("company error : ", error)
      })
  }


  fetchUserDetails = (endpoint) => {
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    postWithToken(employeeApiUrl, endpoint, paramData)
      .then((resp) => {
        if (resp.Status == true) {
          // showMsg(resp.msg);
          if (resp.Data) {
            console.log("Employee BasicInfo from dashboard===> ", resp?.Data);
            console.log("================")
            dispatch(setEmpBasicDetails(resp.Data));
            getAvailableModule()
          } else {
            dispatch(setEmpBasicDetails([]))
            showMsg(resp.msg)
          }
        } else {
          dispatch(setEmpBasicDetails([]))
          showMsg(resp.msg);
          singOutFunc()
        }
      })
      .catch((error) => {
        dispatch(setEmpBasicDetails([]))
        console.log("Employee BasicInfo api error from dashboard: ", error)
      })
  }

  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  //===========GET CURRENT DATE AND TIME ================
  const getCurrentDateTime = (() => {
    const today = new Date();
    const yyyy = today.getFullYear();
    let mm = today.getMonth() + 1; // Months start at 0!
    let dd = today.getDate();
    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    let fullDate = dd + '-' + mm + '-' + yyyy;
    let currTime = ("0" + today.getHours()).slice(-2) + ":" + ("0" + today.getMinutes()).slice(-2) + ":" + ("0" + today.getSeconds()).slice(-2);
    setTime(currTime)
    setDate(fullDate)
  })

  /*== Logout Button click function === */
  const singOutBtn = () => {
    showConfirmDialog("Logout", "Are you sure you want to logout from this app?", "Logout", "LOGOUT")
  }

  /*== Logout functionality start === */
  const singOutFunc = () => {
    dispatch(clearEmpAndCompanyDetails())
    deleteData();
    setTimeout(() => {
      props.navigation.replace('CompanyLogin');
    }, 400);
  }

  /* == Show logout confirmation alert ==*/
  const showConfirmDialog = (title, body, actionText, type) => {
    return Alert.alert(
      title,
      body,
      [
        {
          text: actionText, onPress: () => {
            type == 'LOGOUT' ? singOutFunc() : BackHandler.exitApp();
          },
        },
        {
          text: "Cancel",
        },
      ]
    );
  };

  const goNextScreen = (res) => {
    if (res?.Module == "Attendance") {
      props.navigation.navigate('AttendanceDashboard', { pData: res?.Submenu })
    }else if(res?.Module == "MyProfile"){
      props.navigation.navigate('ProfileScreen', { pData: res?.Submenu })
    }else if(res?.Module == "LeaveDetails"){
      props.navigation.navigate('LeavelistScreen', { pData: res?.Submenu })
    }else if(res?.Module == "Payslip"){
      props.navigation.navigate('PayslipScreen', { pData: res?.Submenu })
    }else if(res?.Module == "MyExpenses"){
      props.navigation.navigate('ExpanseScreen', { pData: res?.Submenu })
    }else if(res?.Module == "MyAssets"){
      props.navigation.navigate('AssetsScreen', { pData: res?.Submenu })
    }else if(res?.Module == "Loan"){
      props.navigation.navigate('LoanMainScreen', { pData: res?.Submenu })
    }
    
  }

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Dashboard"
      headerbackClick={() => { props.navigation.goBack() }}
      hamburgmenuVisable={true}
      // headermenuClick = {()=>{props.navigation.dispatch(DrawerActions.openDrawer())}}
      showpowerButton={true}
      clickPowerbutton={() => { singOutBtn() }}
    >
      <View style={styles.container}>
        {/* ================ PROFILE CARD START  ==================== */}
        <View style={styles.ProfileCard}>
          <Image source={Images.dummy_user_image} style={styles.profileImage} />
          <Text style={styles.userName}>{employeeDetails?.EmployeeName}</Text>
          <Text style={styles.time}>{fullDate}  | {currentTime}</Text>
        </View>
        {/* ================ PROFILE CARD END  ==================== */}
        {moduleData != "" ?
          <View style={styles.menuCard}>
            <FlatList
              data={moduleData.filter(module => module.IsActive)}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.rightBox}
                  onPress={() =>
                    // props.navigation.navigate('ProfileScreen')
                    goNextScreen(item)
                  }>
                  <Image source={{ uri: item?.AndroidIconPath }} style={styles.cardIcon} />
                  <Text style={styles.cardText}>{item?.ModuleName}</Text>
                </TouchableOpacity>
              )}
              // keyExtractor={(item) => item.id}
              numColumns={2}
              contentContainerStyle={styles.listContainer}
            />

          </View> : null}
      </View>

      <Modal
        transparent={true}
        animationType={'none'}
        visible={loader}
        style={{ zIndex: 1100 }}
        onRequestClose={() => { }}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={loader} color="black" size={30} />
          </View>
        </View>
      </Modal>
    </ScreenLayout>
  );
};

export default Dashboard;
//props.navigation.navigate('DashDetails')