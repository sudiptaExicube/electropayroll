//import liraries
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { FontFamily, FontSize } from '../../Constants/Fonts';
import { useTheme } from '../../Constants/Theme/Theme';
import { windowHeight, windowWidth } from '../../Constants/window';


// create a component
const Styles = () => {
  const { colorTheme } = useTheme();
  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      flexDirection: 'column',
      //backgroundColor: colorTheme.buttonBackgroundColor,
      backgroundColor: "#051121",
      height: windowHeight,

    },
    profileImage: {
      height: 65,
      width: 65,
    },
    userName: {
      fontSize: FontSize.f15,
      marginTop: 6,
      color: colorTheme.buttonTextColor,
      textTransform: 'uppercase',
      fontFamily:FontFamily.bold
    },
    time: {
      color: colorTheme.buttonTextColor,
      fontSize: FontSize.f12,
    },
    ProfileCard: {
      width: "100%",
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 12
      // backgroundColor: colorTheme.buttonBackgroundColor,
    },
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#rgba(0, 0, 0, 0.5)',
      zIndex: 1000
    },
    activityIndicatorWrapper: {
      backgroundColor: '#FFFFFF',
      height: 40,
      width: 40,
      borderRadius: 20,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    menuCard:{
      width: windowWidth,
      borderTopLeftRadius: 12,
      borderTopRightRadius: 12,
      flex: 1,
      height:'100%',
      // justifyContent:'flex-start',
      // alignItems:'center',
      backgroundColor: colorTheme.backgroundColor,
      flexDirection:'column',
      paddingTop:18,
      paddingLeft:10
    },
    boxContainer:{
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'row',
      marginTop:8,
      marginLeft:18
    },
    leftBox: {
      width: "45%",
      height: 110,
      paddingVertical: 8,
      paddingHorizontal: 8,
      justifyContent: 'center',
      alignItems:'center',
      backgroundColor: colorTheme.shadeLight,
      borderRadius:12,
      marginRight:5,
    },
    rightBox: {
      width: "47%",
      height: 125,
      marginRight:5,
      paddingVertical: 8,
      paddingHorizontal: 8,
      justifyContent: 'center',
      alignItems:'center',
      backgroundColor: colorTheme.shadeLight,
      borderRadius:12,
      marginRight:10,
      marginBottom:10
    },
    cardIcon:{
      height: 40,
      width: 40,
    },
    cardText:{
      fontSize:FontSize.f15,
      color:colorTheme.blackColor,
      marginTop:4
    }
  });
};
export default Styles;
