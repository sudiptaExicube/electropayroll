import React, { Component, useState, useEffect } from "react";
import { View, Text, StyleSheet, TouchableOpacity,ScrollView, FlatList, Modal, Image, ToastAndroid, RefreshControl, ActivityIndicator } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { useTheme } from '../../Constants/Theme/Theme';
import Styles from './Style';
import { ScreenLayout } from '../../Components';
import Icon from 'react-native-vector-icons/Ionicons';

import { Icons, Images } from '../../Constants/ImageIconContant';
import { useDispatch, useSelector } from 'react-redux';
import { getDataWithToken, postWithToken } from '../../Service/service';
import {
  setleaveBalance, clearleaveBalance,
  setleaveApplicationHistory, clearleaveApplicationHistory
} from '../../Store/Reducers/LeaveReducer';

// create a component
const ApplyApplicationScreen = props => {

  const { colorTheme } = useTheme()
  const styles = Styles();
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(true);
  const { employeeDetails, employeeApiUrl, token } = useSelector(state => state.common);
  const { leaveBalance, leaveApplicationHistory } = useSelector(state => state.leave);
  const [refreshing, setRefreshing] = React.useState(false);
  const [loader, setloader] = React.useState(false);

  const isFocused = useIsFocused();
  useEffect(() => {
    if (isFocused == true) {
      fetchLeaveApplicationHistory(null)
    }

  }, [isFocused])

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      fetchLeaveApplicationHistory('pullToRefresh')
      setRefreshing(false)
    });
  }, []);

  fetchLeaveApplicationHistory = (event) => {
    event == null ? setloader(true) : setloader(false)
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    postWithToken(employeeApiUrl, '/MyLeaveApplications', paramData)
      .then((resp) => {
        event == null ? setloader(false) : setloader(false)
        if (resp.Status == true) {
          // showMsg(resp.msg);
          if (resp.Data) {
            console.log("Leave Application history ===> ", resp?.Data);
            console.log("================")
            dispatch(setleaveApplicationHistory(resp.Data))
            //setModalVisible(!modalVisible);
          } else { dispatch(setleaveApplicationHistory([])) }
        } else {
          dispatch(setleaveApplicationHistory([]))
          showMsg(resp.msg)
        }
      })
      .catch((error) => {
        console.log("Leave balance api error : ", error)

      })
  }

  /* == Show Toast msg function == */
  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  const renderItem = ({ item }) => (
    <View style={styles.listMainContainer}>
      <Text style={styles.listHeader}>{item?.LeaveType}</Text>
      <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 5 }}>
        <Icon name="calendar-outline" size={18} color="#696969" />
        <Text style={styles.leaveRange}>{item?.FromDate} - {item?.ToDate} ({item?.NoOfDays} {item?.NoOfDays > 1 ? 'Days' : 'Day'})</Text>
      </View>
      <Text numberOfLines={2} style={styles.leaveReason}>{item?.Reason}</Text>
      <View style={styles.border}></View>
      <View style={styles.listFootersec}>
        <View style={styles.statusContainer}>
          <View style={[styles.statusIcon, { backgroundColor: colorTheme.lightBlue }]}></View>
          <Text style={[styles.statusText, { color: colorTheme.lightBlue }]}>{item?.ApplicationStatus}</Text>
        </View>
        <View><Text style={styles.applyDate}>{item?.ApplicationDate}</Text></View>
      </View>
    </View>
  );

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="Leave Applications"
      headerbackClick={() => { props.navigation.goBack() }}>
      <View
        style={styles.container}>
        {leaveApplicationHistory.length > 0 ?
          <FlatList
            data={leaveApplicationHistory}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10, borderColor: '#004792', marginBottom: 80 }}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          /> :
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          >
            <Text style={{ marginTop: 180, fontSize: 15, textAlign: 'center' }}>Sorry! No Result Found</Text>
          </ScrollView>
        }
      </View>
      <Modal
        transparent={true}
        animationType={'none'}
        visible={loader}
        style={{ zIndex: 1100 }}
        onRequestClose={() => { }}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={loader} color="black" size={30} />
          </View>
        </View>
      </Modal>
    </ScreenLayout>
  );
};

//make this component available to the app
export default ApplyApplicationScreen;
