export {default as Initial} from './Initial/Initial'
export {default as Dashboard} from './Dashboard/Dashboard'
export {default as DashDetails} from './DashboardDetails/DashboardDetails'

// Authentication screen
export {default as CompanyLogin} from './authentication-screens/company-login/Companylogin';
export {default as EmployeeLogin} from './authentication-screens/employee-login/Emplogin';

//Other screen
export {default as ProfileScreen} from './profile-screen/ProfileScreen';
export {default as PayslipScreen} from './payslip-screen/PayslipScreen';
export {default as LeavelistScreen} from './leave-screen/LeavelistScreen';
export {default as ApplyLeaveScreen} from './apply-leave/ApplyleaveScreen';
export {default as ApplyApplicationScreen} from './my-application/MyapplicationScreen';
export {default as LeaveBalanceScreen} from './leave-balance/LeaveBalanceScreen';
export {default as ApproveLeaveScreen} from './leave-approve/LeaveapproveScreen';


export {default as AttendanceDashboard} from './attendance-screen/AttendanceDashboard';
export {default as AttendanceScreen} from './attendance-screen/AttendanceScreen';
export {default as AttendanceRepport} from './attendance-screen/AttendanceRepport'
export {default as OTApplication} from './attendance-screen/OTApplication'

export {default as ExpanseScreen} from './expanse-screen/ExpanseScreen';
export {default as AddexpanseScreen} from './add-expanse/AddexpanseScreen';

export {default as AssetsScreen} from './assets-screen/AssetsScreen';

export {default as ApplyLoanScreen} from './apply-loan/ApplyLoanScreen'
export {default as LoanMainScreen} from './LoanMainScreen/LoanMainScreen'


export {default as Holiday} from './holiday/Holiday'
export {default as Regularization} from './attendance-screen/regularization/Regularization'



