import React, { Component, useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  ScrollView,
  SafeAreaView,
  Image,
  ToastAndroid,
  RefreshControl,
  ActivityIndicator,
  Modal
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import {
  setAssetList,
  clearAssetList
} from '../../Store/Reducers/AssetsReducer';
import { getDataWithOutToken, getDataWithToken, postWithToken } from '../../Service/service';

import { useTheme } from '../../Constants/Theme/Theme';
import Styles from './Style';
import { ScreenLayout } from '../../Components';
import { useIsFocused } from '@react-navigation/native';
import { Images } from '../../Constants/ImageIconContant';
import Icon from 'react-native-vector-icons/Ionicons';
import { FontFamily } from '../../Constants/Fonts';

// create a component
const AssetsScreen = props => {
  const { colorTheme } = useTheme()
  const styles = Styles();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const [loader, setloader] = React.useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const { employeeDetails, employeeApiUrl, token } = useSelector(state => state.common);
  const { assetsList } = useSelector(state => state.assets);

  useEffect(() => {
    if (isFocused == true) {
      fetchAssetsHistory(null)
    }
  }, [isFocused])

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => {
      fetchAssetsHistory('pullToRefresh')
      setRefreshing(false)
    });
  }, []);

  fetchAssetsHistory = (event) => {
    event == null ? setloader(true) : setloader(false)
    let paramData = {
      EmployeeId: employeeDetails?.EmployeeId,
      Token: token
    }
    postWithToken(employeeApiUrl, '/MyAssets', paramData)
      .then((resp) => {
        event == null ? setloader(false) : setloader(false)
        if (resp.Status == true) {
          // showMsg(resp.msg);
          if (resp.Data) {
            console.log("Assets List ===> ", resp);
            console.log("================");
            dispatch(setAssetList(resp.Data));
          } else { dispatch(setAssetList([])) }
        } else {
          //dispatch(setAssetList([]))
          showMsg(resp.msg)
        }
      })
      .catch((error) => {
        console.log("get Assets api error : ", error)
        //dispatch(setAssetList([]))
      })
  }

  /* == Show Toast msg function == */
  const showMsg = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT)
  }

  const renderItem = ({ item }) => (
    <View style={[styles.detailsCardMain, { marginTop: 6 }]}>
      <View style={styles.detailsCardWrapper}>
        {item?.IsReturned != 'NO' ? <View style={styles.headingMain}>
          <View style={styles.headerBullet}></View>
          <Text style={styles.headerTitle}>Returented</Text>
        </View> : null}
        <Text style={[styles.key,{marginTop:item?.IsReturned != 'NO' ? 0 : 10}]}>Asset Reference No</Text>
        <Text style={styles.value}>{item?.AssetReferenceNo}</Text>

        <Text style={styles.key}>Asset Name</Text>
        <Text style={styles.value}>{item?.AssetName ? item?.AssetName : "--"}</Text>

        <Text style={styles.key}>SerialNo</Text>
        <Text style={styles.value}>{item?.SerialNo ? item?.SerialNo : "--"}</Text>

        <Text style={styles.key}>Brand Name</Text>
        <Text style={styles.value}>{item?.Brand ? item?.Brand : "--"}</Text>

        <Text style={styles.key}>Model Name</Text>
        <Text style={styles.value}>{item?.Model ? item?.Model : "--"}</Text>

        <Text style={styles.key}>Issue Date</Text>
        <Text style={styles.value}>{item?.IssueDate ? item?.IssueDate : "--"}</Text>

        <Text style={styles.key}>Expected Return Date</Text>
        <Text style={styles.value}>{item?.ExpectedReturnDate ? item?.ExpectedReturnDate : "--"}</Text>

        <Text style={styles.key}>Issue Note</Text>
        <Text style={styles.value}>{item?.IssueNote ? item?.IssueNote : "--"}</Text>


      </View>
    </View>
  );

  return (
    <ScreenLayout
      isHeaderShown={true}
      isShownHeaderLogo={false}
      headerTitle="My Assets"
      headerbackClick={() => { props.navigation.goBack() }}
      showpowerButton={false}
    >
      <View
        style={styles.container}>
        {assetsList.length > 0 ?
          <FlatList
            data={assetsList}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10, borderColor: '#004792', marginBottom: 90 }}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          /> :
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
          >
            <Text style={{ marginTop: 180, fontSize: 15, textAlign: 'center' }}>Sorry! No Result Found</Text>
          </ScrollView>
        }
      </View>
      <Modal
        transparent={true}
        animationType={'none'}
        visible={loader}
        style={{ zIndex: 1100 }}
        onRequestClose={() => { }}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={loader} color="black" size={30} />
          </View>
        </View>
      </Modal>
    </ScreenLayout>
  );
};

//make this component available to the app
export default AssetsScreen;
